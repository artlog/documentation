- Installer PeerTube

  - [Any OS (recommended)](install-any-os.md)
  - [Docker](install-docker.md)
  - [Unofficial](install-unofficial.md)


- Maintain PeerTube (sysadmin)

 - [CLI tools](maintain-tools.md)
 - [Instance migration](maintain-migration.md)
 - [Cache](maintain-cache.md)
 - [Security](maintain-security.md)
 - [Configuration](maintain-configuration.md)


- Administrer PeerTube

 - [Following other instances](admin-following-instances.md)
 - [Managing users](admin-managing-users.md)


- Utiliser PeerTube

 - [Setup your account](use-setup-account.md)
 - [Sharing a video](use-share-video.md)
 - [Télécharger une vidéo](fr/use-download-video.md)
 - [Video channels](use-video-channels.md)
 - [Video playlists](use-video-playlists.md)
 - [Video history](use-video-history.md)
 - [Search](use-search.md)

- Contribuer à PeerTube

 - [Getting started](contribute-getting-started.md)
 - [Architecture](contribute-architecture.md)
 - [Code of conduct](contribute-code-of-conduct.md)

- PeerTube API

 - [Getting started with REST API](api-rest-getting-started.md)
 - [REST API reference](api-rest-reference.html)
 - [ActivityPub](api-activitypub.md)
