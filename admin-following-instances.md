# Following other instances

Following servers and being followed as a server ensure visibility of your videos on other instances, and visibility of their videos on your instance. Both are important concepts of PeerTube as they allow instances and their users to interact.

!> **What is a "follow":** a follow is [a kind of activity](https://www.w3.org/TR/activitypub/#follow-activity-inbox) in the ActivityPub linguo. It allows to subscribe to a server's user activity in the PeerTube realm.

## Following an instance

Following an instance will display videos of that instance on your pages (i.e. "Trending", "Recently Added", etc.) and your users will be able to interact with them.
https://docs.joinpeertube.org/lang/en/docs/install.html#git-stable

## Managing follows

You can add an instance to follow and remove instances you follow in `Administration > Manage Follows > Follow`, and add hostnames of the instances you want to follow there.

![Adding servers to follow](/assets/admin-add-follow.png)

## Being followed by an instance

Being followed will display videos of your instance on your followers' pages, and their users will be able to interact with your videos.

You cannot yet refuse a follow as it is automatically accepted, but you can block the instance _a posteriori_.
